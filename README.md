# Customer Direct's Ruby Style Guide

_A mostly reasonable approach to Ruby_

Follow [Bozhidar Batsov's](https://github.com/bbatsov/ruby-style-guide) guide.

## Installation

1. Install `rubocop`
2. Copy the `.rubocop.yml` into the root of your project directory.

## Inline Linting

* Sublime - [SublimeLinter-rubocop](https://packagecontrol.io/packages/SublimeLinter-rubocop)
* VIM - [vim-rubocop](http://vimawesome.com/plugin/vim-rubocop)
* Atom - [linter-rubocop](https://atom.io/packages/linter-rubocop)
* RubyMine - [Rubocop](https://plugins.jetbrains.com/plugin/7604?pr=ruby)